#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include "RsaPrivPacket.h"
 
 
 
#define INTARRAYSIZE 10
#define MAXPKTSIZE   256
 
 

 
/* Create a structure that matches the main ASN.1 declaration */
 
typedef struct MyRsaPrivPacket {
        INTEGER_t        version;
        INTEGER_t        modulus;
        INTEGER_t        publicExponent;
        INTEGER_t        privateExponent;
        INTEGER_t        prime1;
        INTEGER_t        prime2;
        INTEGER_t        exponent1;
        INTEGER_t        exponent2;
        INTEGER_t        coefficient;

        /* Context for parsing across buffer boundaries */
        asn_struct_ctx_t _asn_ctx;
} MyRsaPrivPacket;

int main ()
{
	MyRsaPrivPacket *pkt;
	printf("hoi \n");
	xer_fprint(stdout, &asn_DEF_MyRsaPrivPacket, pkt);

}
